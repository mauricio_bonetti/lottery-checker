package com.bon.lottery.service;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.bon.lottery.config.Context;
import com.bon.lottery.config.ServiceLocator;
import com.bon.lottery.mock.DAOLocatorMock;
import com.bon.lottery.vo.BetVO;
import com.bon.lottery.vo.Lottery;
import com.bon.lottery.vo.ResultVO;


public class LotteryServiceTest {
	@Before
	public void setup() {
		Context.injectBeans(ServiceLocator.class, DAOLocatorMock.class);
	}
	
	// TODO Give a meanngfull name
	@Test
	public void findResultsTest1() {
		LotteryService service = Context.loadBean(LotteryService.class);
		List<BetVO> bets = loadBets();
		List<ResultVO> results = service.findResults(bets);
		Assert.assertEquals(results.size(), bets.size());
	}
	
	@Test(expected=RuntimeException.class)
	public void findResultsTest2() {
		LotteryService service = Context.loadBean(LotteryService.class);
		try {
			service.findResults(null);
		} catch (RuntimeException e) {
			service.findResults(new ArrayList<BetVO>());
		}
	}

	private List<BetVO> loadBets() {
		List<BetVO> bets = new ArrayList<BetVO>();
		bets.add(new BetVO(Lottery.LOTOFACIL, 1010, "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15"));
		bets.add(new BetVO(Lottery.LOTOFACIL, 2020, "2 3 4 5 6 7 8 9 10 11 12 13 14 15 16"));
		return bets;
	}
}
