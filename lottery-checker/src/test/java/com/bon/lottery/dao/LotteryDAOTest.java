package com.bon.lottery.dao;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.bon.lottery.config.Context;
import com.bon.lottery.config.DAOLocator;
import com.bon.lottery.vo.BetVO;
import com.bon.lottery.vo.Lottery;
import com.bon.lottery.vo.ResultVO;

public class LotteryDAOTest {
	@Before
	public void setup() {
		Context.injectBeans(DAOLocator.class);
	}
	
	@Test
	public void findResultTest() {
		BetVO bet = new BetVO(Lottery.LOTOFACIL, 1069, "22 23 17 15 01 11 16 04 08 12 03 10 13 25 24");
		ResultVO expectedResult = new ResultVO(bet, 15);
		LotteryDAO dao = Context.loadBean(LotteryDAO.class);
		ResultVO actualResult = dao.findResult(bet);
		Assert.assertEquals(expectedResult, actualResult);
		
	}
}
