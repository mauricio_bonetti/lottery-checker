package com.bon.lottery.util;

import junit.framework.Assert;

import org.junit.Test;

import com.bon.lottery.vo.Lottery;

public class ValidationUtilTest {
	@Test
	public void isBetValidTest() {
		isLotoFacilBetValidTest();
	}

	private void isLotoFacilBetValidTest() {
		String bet1 = "02 03 05 06 07 08 11 12 14 15 17  18 20 21 24";
		boolean betResult = ValidationUtil.isBetValid(Lottery.LOTOFACIL, bet1);
		Assert.assertTrue("A aposta " + bet1 + " é uma aposta válida para LotoFacil", betResult);
		
		String bet2 = "01 03 04 06 07 09 10 11 13 14 17 19 21 22 25";
		betResult = ValidationUtil.isBetValid(Lottery.LOTOFACIL, bet2);
		Assert.assertTrue("A aposta " + bet2 + " é uma aposta válida para LotoFacil", betResult);
		
		String bet3 = "01 03 04 06 07 09 10 11 13 14 17 19 21 22 25 02 05 12";
		betResult = ValidationUtil.isBetValid(Lottery.LOTOFACIL, bet3);
		Assert.assertTrue("A aposta " + bet3 + " é uma aposta válida para LotoFacil", betResult);
		
		String bet4 = "01 03 04 06 07 09 10 11 13 14 17 19 21 22 25 02 05 12 08";
		betResult = ValidationUtil.isBetValid(Lottery.LOTOFACIL, bet4);
		Assert.assertFalse("A aposta " + bet4 + " é uma aposta inválida para LotoFacil", betResult);
		
		String bet5 = "01 03 04 06 07 09 10 11 13 14 17 19 21 22 26";
		betResult = ValidationUtil.isBetValid(Lottery.LOTOFACIL, bet5);
		Assert.assertFalse("A aposta " + bet5 + " é uma aposta inválida para LotoFacil", betResult);
		
	}
}
