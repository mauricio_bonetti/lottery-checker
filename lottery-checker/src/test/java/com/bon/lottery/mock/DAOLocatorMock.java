package com.bon.lottery.mock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bon.lottery.dao.LotteryDAO;
import com.bon.lottery.vo.BetVO;
import com.bon.lottery.vo.ResultVO;
@Configuration
public class DAOLocatorMock {
	@Bean
	public LotteryDAO getLotteryDAO() {
		return new LotteryDAOMockImpl();
	}
	
	public class LotteryDAOMockImpl implements LotteryDAO {
		public ResultVO findResult(BetVO bet) {
			return new ResultVO(bet, 10);
		}
	}
}
