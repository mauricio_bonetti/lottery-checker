package com.bon.lottery.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bon.lottery.vo.BetVO;
import com.bon.lottery.vo.Lottery;

public final class InputUtil {
	public static List<BetVO> readBets() {
		Lottery lottery = readLottery();
		int contest = readContest();
		List<String> betSequences = readBetSequences(lottery);
		List<BetVO> bets = new ArrayList<BetVO>();
		for (String betSequence : betSequences) {
			bets.add(new BetVO(lottery, contest, betSequence));
		}
		return bets;
	}

	private static List<String> readBetSequences(Lottery lottery) {
		List<String> bets = new ArrayList<String>();
		bets.add(readBet(lottery));
		for (;;) {
			System.out.println("Deseja incluir outra aposta? <S,N> e presione <ENTER>");
			char option = new Scanner(System.in).next().charAt(0);
			// TODO: Refactor
			if (option == 'N' || option == 'n') {
				break;
			}
			bets.add(readBet(lottery));
		}
		return bets;
	}

	private static String readBet(Lottery lottery) {
		System.out.println("Digite uma aposta separada por espaço e presione <ENTER>");
		String bet = new Scanner(System.in).nextLine();
		if (ValidationUtil.isBetValid(lottery, bet)) {
			return bet;
		}
		System.out.println("Aposta inválida");
		return null;
	}

	private static int readContest() {
		System.out.println("Digite o número do concurso e pressione <ENTER>");
		return new Scanner(System.in).nextInt();
	}

	private static Lottery readLottery() {
		System.out.println("***** Escolha a Loteria *****");
		System.out.println("1 - Mega-Sena");
		System.out.println("2 - Lotomania");
		System.out.println("3 - Loteca");
		System.out.println("4 - Quina");
		System.out.println("5 - Dupla-Sena");
		System.out.println("6 - Lotogol");
		System.out.println("7 - Lotofácil");
		System.out.println("8 - Loteria-Federal");
		System.out.println("9 - Timemania");
		System.out.println("Digite a sua escolha e pressione <ENTER>");
		
		int v = new Scanner(System.in).nextInt();
		switch (v) {
			case 1: return Lottery.MEGASENA;
			case 2: return Lottery.LOTOMANIA;
			case 3: return Lottery.LOTECA;
			case 4: return Lottery.QUINA;
			case 5: return Lottery.DUPLASENA;
			case 6: return Lottery.LOTOGOL;
			case 7: return Lottery.LOTOFACIL;
			case 8: return Lottery.LOTERIAFEDERAL;
			case 9: return Lottery.TIMEMANIA;
			default: throw new RuntimeException("Loteria Inválida"); // TODO: Fazer tratamento para a aplicação não encerrar.
		}
	}
}
