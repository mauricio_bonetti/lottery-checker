package com.bon.lottery.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileUtil {
	private static final int BUFFER_SIZE = 4096;

	public static void downloadContestFile(String url) {
		try {
			// TODO: Delete previous file
			URL u = new URL(url);
			CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
			HttpURLConnection conn = (HttpURLConnection) u.openConnection();
			int httpCode = conn.getResponseCode();
			if (httpCode == HttpURLConnection.HTTP_OK) {
				String fileName = "";
				String disposition = conn.getHeaderField("Content-Disposition");
				String contentType = conn.getContentType();
				int contentLength = conn.getContentLength();
				/*
				 * // TODO: Log content below
				 * System.out.println("Content-Type = " + contentType);
				 * System.out.println("Content-Disposition = " + disposition);
				 * System.out.println("Content-Length = " + contentLength);
				 * System.out.println("fileName = " + fileName);
				 */
				InputStream is = conn.getInputStream();
				FileOutputStream os = new FileOutputStream("tmp"
						+ File.separator + "result.zip");

				int bytesRead = -1;
				byte[] buffer = new byte[BUFFER_SIZE];
				while ((bytesRead = is.read(buffer)) != -1) {
					os.write(buffer, 0, bytesRead);
				}
				os.close();
				is.close();
			} else {
				throw new RuntimeException("Error. Server replied HTTP code: "
						+ httpCode);
			}
			conn.disconnect();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	// TODO: ADD logs!
	public static String readContestFile() {
		String filename = "tmp" + File.separator + "result.zip";
		StringBuilder content = new StringBuilder();
		try {
			ZipInputStream in = new ZipInputStream(new FileInputStream(
					filename));
			ZipEntry entry = in.getNextEntry();
			while (entry != null && entry.getName().contains(".HTM")) {
				byte[] buf = new byte[(int) entry.getSize()];
				int len;
				while ((len = in.read(buf)) != -1) {
					content.append(new String(buf, 0, len));
				}
				entry = in.getNextEntry();
			}
			in.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return content.toString();
	}

}