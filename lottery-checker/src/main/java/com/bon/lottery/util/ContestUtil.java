package com.bon.lottery.util;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.bon.lottery.vo.BetVO;
import com.bon.lottery.vo.Lottery;

public final class ContestUtil {

	public static List<BetVO> loadContestResults(String htmlContestResult, Lottery lottery) {
		Document doc = Jsoup.parse(htmlContestResult);
		Elements contests = doc.getElementsByTag("tr");
		List<BetVO> results = new ArrayList<BetVO>();
		for (int i = 1; i < contests.size(); i++) {
			Element result = contests.get(i);
			Elements elements = result.getElementsByAttributeStarting("rowspan");
			if (elements.size() <= 0) {
				continue;
			}
			String contest = elements.get(0).html();
			StringBuilder betSequence = new StringBuilder();
			for (int j = 2; j < 17; j++) {
				if (j != 2) {
					betSequence.append(" ");
				}
				try {
					betSequence.append(elements.get(j).html());
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			results.add(new BetVO(lottery, Integer.parseInt(contest), betSequence.toString()));
		}
		return results;
	}

}
