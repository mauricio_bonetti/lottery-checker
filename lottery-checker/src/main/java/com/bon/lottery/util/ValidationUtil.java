package com.bon.lottery.util;

import org.apache.commons.lang3.StringUtils;

import com.bon.lottery.vo.Lottery;

public final class ValidationUtil {
	public static boolean isBetValid(Lottery lottery, String bet) {
		switch (lottery) {
			case LOTOFACIL: return isLotoFacilBetValid(bet);
		}
		return false;
	}

	private static boolean isLotoFacilBetValid(String bet) {
		if (StringUtils.isNotBlank(bet)) {
			String[] tokens = bet.split("\\s+");	// Quebra por espaço em branco
			if (tokens.length >= 15 && tokens.length <= 18) {
				for (String token : tokens) {
					int t = Integer.parseInt(token);
					if (t < 1 || t > 25) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

}
