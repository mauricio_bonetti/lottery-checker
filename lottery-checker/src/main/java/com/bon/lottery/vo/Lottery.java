package com.bon.lottery.vo;

public enum Lottery {
	MEGASENA("Mega Sena"), LOTOMANIA("Lotomania"), LOTECA("Loteca"), 
	QUINA("Quina"), DUPLASENA("Dupla Sena"), LOTOGOL("Lotogol"),
	LOTOFACIL("Lotofacil"), LOTERIAFEDERAL("Loteria Federal"), TIMEMANIA("Timemania");
	
	private String name; 
	
    private Lottery(String name) { 
        this.name = name; 
    } 
    
    @Override 
    public String toString(){ 
        return name; 
    } 
}
