package com.bon.lottery.vo;

public class BetVO {
	private Lottery lottery;
	private int contest;
	private String betSequence;
	
	public BetVO(Lottery lottery, int contest, String betSequence) {
		this.lottery = lottery;
		this.contest = contest;
		this.betSequence = betSequence;
	}

	public Lottery getLottery() {
		return lottery;
	}

	public int getContest() {
		return contest;
	}

	public String getBetSequence() {
		return betSequence;
	}

	// TODO Refactor
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof BetVO)) {
			return false;
		}
		BetVO other = (BetVO) obj;
		if (betSequence == null) {
			if (other.betSequence != null) {
				return false;
			}
		} else if (!betSequence.equals(other.betSequence)) {
			return false;
		}
		if (contest != other.contest) {
			return false;
		}
		if (lottery != other.lottery) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Loteria: ");
		sb.append(lottery.toString());
		sb.append("\n");
		sb.append("Concurso: ");
		sb.append(contest);
		sb.append("\n");
		sb.append("Aposta: ");
		sb.append(betSequence);
		return sb.toString();
	}
	
}