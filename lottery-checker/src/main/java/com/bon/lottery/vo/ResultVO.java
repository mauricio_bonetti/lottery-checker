package com.bon.lottery.vo;

public class ResultVO {
	private BetVO bet;
	private int score;
	
	public ResultVO(BetVO bet, int score) {
		this.bet = bet;
		this.score = score;
	}

	public BetVO getBet() {
		return bet;
	}

	public int getScore() {
		return score;
	}

	// TODO Refactor
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ResultVO)) {
			return false;
		}
		ResultVO other = (ResultVO) obj;
		if (bet == null) {
			if (other.bet != null) {
				return false;
			}
		} else if (!bet.equals(other.bet)) {
			return false;
		}
		if (score != other.score) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(bet.toString());
		sb.append("\n");
		sb.append("Resultado: ");
		sb.append(score);
		sb.append(" pontos");
		return sb.toString();
	}
}
