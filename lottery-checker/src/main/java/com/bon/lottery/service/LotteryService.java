package com.bon.lottery.service;

import java.util.List;

import com.bon.lottery.vo.BetVO;
import com.bon.lottery.vo.ResultVO;

public interface LotteryService {
	public List<ResultVO> findResults(List<BetVO> bets);
}
