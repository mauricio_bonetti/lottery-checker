package com.bon.lottery.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.bon.lottery.config.Context;
import com.bon.lottery.dao.LotteryDAO;
import com.bon.lottery.vo.BetVO;
import com.bon.lottery.vo.ResultVO;

public class LotteryServiceImpl implements LotteryService {
	public List<ResultVO> findResults(List<BetVO> bets) {
		if (CollectionUtils.isEmpty(bets)) {
			throw new RuntimeException("Cannot find result of no bets");
		}
		List<ResultVO> results = new ArrayList<ResultVO>();
		LotteryDAO dao = Context.loadBean(LotteryDAO.class);
		for (BetVO bet : bets) {
			results.add(dao.findResult(bet));
		}
		return results;
	}

}
