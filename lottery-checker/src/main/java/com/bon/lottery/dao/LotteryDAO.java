package com.bon.lottery.dao;

import com.bon.lottery.vo.BetVO;
import com.bon.lottery.vo.Lottery;
import com.bon.lottery.vo.ResultVO;

public interface LotteryDAO {
	public ResultVO findResult(BetVO bet);
}
