package com.bon.lottery.dao;

import java.util.List;

import com.bon.lottery.util.ContestUtil;
import com.bon.lottery.util.FileUtil;
import com.bon.lottery.vo.BetVO;
import com.bon.lottery.vo.Lottery;
import com.bon.lottery.vo.ResultVO;

public class LotteryDAOImpl implements LotteryDAO {
	// TODO: Refactor this method is too smart to be a DAO
	public ResultVO findResult(BetVO bet) {
		// TODO: Create a cache for results, check it before downloading file
		int score = loadScoreFromFile(bet);
		return new ResultVO(bet, score);
	}
	
	private int loadScoreFromFile(BetVO bet) {
		try {
			FileUtil.downloadContestFile(findContestFileURL(bet.getLottery()));
			List<BetVO> contestResults = ContestUtil.loadContestResults(FileUtil.readContestFile(), bet.getLottery());
			// TODO: Refactor, create a cache for results
			for (BetVO result : contestResults) {
				if (result.getContest() == bet.getContest()) {
					return loadScore(result.getBetSequence(), bet.getBetSequence());
				}
			}
			throw new RuntimeException("No result found");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private int loadScore(String resultSequence, String betSequence) {
		String[] resultValues = resultSequence.split(" ");
		String[] betValues = betSequence.split(" ");
		int score = 0;
		for (String rValue : resultValues) {
			for (String bValue : betValues) {
				if (rValue.equals(bValue)) {
					score++;
				}
			}
		}
		return score;
	}

	private String findContestFileURL(Lottery lottery) {
		switch (lottery) {
			case LOTOFACIL: 
				return "http://www1.caixa.gov.br/loterias/_arquivos/loterias/D_lotfac.zip";
		}
		throw new RuntimeException("Invalid lottery");
	}
}
