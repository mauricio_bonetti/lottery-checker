package com.bon.lottery.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bon.lottery.service.LotteryService;
import com.bon.lottery.service.LotteryServiceImpl;

@Configuration
public class ServiceLocator {
	@Bean
	public LotteryService getLotteryService() {
		return new LotteryServiceImpl();
	}
}
