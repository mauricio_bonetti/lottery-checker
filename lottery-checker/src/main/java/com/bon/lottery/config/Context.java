package com.bon.lottery.config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Context {
	static private ThreadLocal<AnnotationConfigApplicationContext> thread = new ThreadLocal<AnnotationConfigApplicationContext>();

	public static void injectBeans(Class<?>... classes) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		for (Class<?> c : classes) {
			context.register(c);
		}
		context.refresh();
		inject(context);
	}
	
	public static void injectBeans(String... pkgs) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		for (String pkg : pkgs) {
			context.scan(pkg);
		}
		context.refresh();
		inject(context);
	}
	
	public static void injectDefaultBeans() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.scan("com.bon.lottery.config");
		context.refresh();
		inject(context);
	}
	
	private static void inject(AnnotationConfigApplicationContext context) {
		thread.set(context);
	}
	
	private static AnnotationConfigApplicationContext getContext() {
		AnnotationConfigApplicationContext context = thread.get();
		if (context == null) {
			throw new RuntimeException("Context not loaded yet");
		}
		return context;
	}
	
	public static <T extends Object> T loadBean(Class<T> beanClass) {
		AnnotationConfigApplicationContext context = getContext();
		return context.getBean(beanClass);
	}
}
