package com.bon.lottery.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bon.lottery.dao.LotteryDAO;
import com.bon.lottery.dao.LotteryDAOImpl;

@Configuration
public class DAOLocator {
	@Bean
	public LotteryDAO getLotteryDAO() {
		return new LotteryDAOImpl();
	}
}
