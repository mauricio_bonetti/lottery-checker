package com.bon.lottery.app;

import java.util.List;

import com.bon.lottery.config.Context;
import com.bon.lottery.service.LotteryService;
import com.bon.lottery.util.InputUtil;
import com.bon.lottery.vo.BetVO;
import com.bon.lottery.vo.ResultVO;

public class Main {

	public static void main(String[] args) {
		injectBeans();
		runApplication();
	}

	private static void runApplication() {
		List<BetVO> bets = InputUtil.readBets();
		printResults(findResults(bets));
	}

	private static void printResults(List<ResultVO> results) {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		sb.append("*** Resultados ***");
		for (ResultVO result : results) {
			sb.append("\n");
			sb.append(result.toString());
		}
		System.out.println(sb.toString());
	}

	private static List<ResultVO> findResults(List<BetVO> bets) {
		LotteryService lotteryService = Context.loadBean(LotteryService.class);
		return lotteryService.findResults(bets);
	}

	private static void injectBeans() {
		Context.injectDefaultBeans();
	}
}
